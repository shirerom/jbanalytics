//
//  JBAnalytics.h
//  JBAnalytics
//
//  Created by Johannes Bauer on 27.08.2015.
//  Copyright (c) 2014 Johannes Bauer. All rights reserved.
//

#ifndef JBAnalytics_h
#define JBAnalytics_h

#import <Crashlytics/Crashlytics.h>
#import <GoogleAnalytics/GAI.h>
#import <GoogleAnalytics/GAIDictionaryBuilder.h>
#import <GoogleAnalytics/GAIFields.h>


/*!
 Screen
 
 s = screen
 */
#define JBAnalyticsSendView(s) \
[[GAI sharedInstance].defaultTracker set:kGAIScreenName value:s]; \
[[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createScreenView] build]]; \
[[Crashlytics sharedInstance] setObjectValue:s forKey:@"screen"]; \
[Answers logContentViewWithName:s contentType:nil contentId:nil customAttributes:nil]

/*!
 Event
 
 c = category
 e = event
 */
#define JBAnalyticsSendEvent(c, e) \
[[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:c action:e label:nil value:nil] build]]; \
[Answers logCustomEventWithName:c customAttributes:@{@"Event": e}]

/**
 Error
 
 d = description
 f = isFatal
 */
#define JBAnalyticsSendError(d, f) \
[[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createExceptionWithDescription:[NSString stringWithFormat:@"%@ +%li: %@", [[[NSString stringWithFormat:@"%s", __FILE__] componentsSeparatedByString: @"/"] lastObject], (long)__LINE__, d] withFatal:[NSNumber numberWithBool:f]] build]]

#endif
