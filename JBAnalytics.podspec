Pod::Spec.new do |s|
  s.name             = 'JBAnalytics'
  s.version          = '0.1.0'
  s.license          = 'MIT'
  s.homepage         = 'https://bitbucket.org/shirerom/jbanalytics'
  s.authors          = { 'Johannes Bauer' => 'mail@bauerjohannes.de' }
  s.summary          = 'Tools for analysing and tracking'
  s.source           = { :git => 'https://bitbucket.org/shirerom/jbanalytics.git'}
  s.source_files     = '*.{h,m}'
  s.requires_arc     = true
  s.framework        = 'Fabric', 'Crashlytics'
end