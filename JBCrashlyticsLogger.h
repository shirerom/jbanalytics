//
//  JBCrashlyticsLogger.h
//  JBAnalytics
//
//  Created by Johannes Bauer on 27.11.14.
//  Copyright (c) 2014 Johannes Bauer. All rights reserved.
//

#import <CocoaLumberjack/CocoaLumberjack.h>

#define JBCRASHLYTICS_LOGGER_REPLACEMENT_TOKEN @"<$$>"


@interface JBCrashlyticsLogger : DDAbstractLogger <DDLogger>

@property (assign, nonatomic) BOOL colorsEnabled;

+ (void)start;
+ (UIColor*)customColorWithFlag:(DDLogFlag)flag;
+ (NSUInteger)customFlags;
+ (NSDictionary*)levels;
+ (NSDictionary*)settings;

@end
