//
//  JBCrashlyticsLogger.m
//  JBAnalytics
//
//  Created by Johannes Bauer on 27.11.14.
//  Copyright (c) 2014 Johannes Bauer. All rights reserved.
//

#import "JBCrashlyticsLogger.h"
#import "JBCustomLogging.h"


@implementation JBCrashlyticsLogger

- (void)logMessage:(DDLogMessage *)logMessage {

	NSString *logMsg = logMessage.message;

	if (logMsg) {
#ifdef DEBUG

		if ([logMsg length] > [JBCRASHLYTICS_LOGGER_REPLACEMENT_TOKEN length] &&
				[[logMsg substringToIndex:[JBCRASHLYTICS_LOGGER_REPLACEMENT_TOKEN length]] isEqualToString:JBCRASHLYTICS_LOGGER_REPLACEMENT_TOKEN]) {
			logMsg = [JBCrashlyticsLogger replaceApiConstantsWithFieldNamesInString:[logMsg substringFromIndex:JBCRASHLYTICS_LOGGER_REPLACEMENT_TOKEN.length]];
			logMsg = [JBCrashlyticsLogger replaceTimestampsWithPrettyDateInString:logMsg];
		}

		if (_colorsEnabled == YES) {

			CGFloat redValue;
			CGFloat greenValue;
			CGFloat blueValue;

			UIColor *textColor = [JBCrashlyticsLogger customColorWithFlag:logMessage.flag];

			if (textColor == nil) {

				switch ((JBLogFlag)(logMessage.flag)) {

					case JBLogFlagError:
						textColor = [UIColor redColor];
						break;

					case JBLogFlagWarning:
						textColor = [UIColor orangeColor];
						break;

					case JBLogFlagDebug:
						textColor = [UIColor grayColor];
						break;

					case JBLogFlagVerbose:
						textColor = [UIColor purpleColor];
						break;

					default:
						textColor = [UIColor blackColor];
						break;
				}
			}

			[textColor getRed:&redValue green:&greenValue blue:&blueValue alpha:nil];

			CLSNSLog(@"\033[fg%i,%i,%i;" @"%@ line %lu $ %@" @"\033[;",
							 (int)(255*redValue), (int)(255*greenValue), (int)(255*blueValue),
							 logMessage.function, (unsigned long)logMessage.line, logMsg);
		}

		else {
			CLSNSLog(@"%@ line %lu $ %@", logMessage.function, (unsigned long)logMessage.line, logMsg);
		}
#else
		CLSLog(@"%@ line %lu $ %@", logMessage.function, (unsigned long)logMessage.line, logMsg);
#endif
	}
}

- (id<DDLogFormatter>)logFormatter {
	return self.logFormatter;
}

- (void)setLogFormatter:(id<DDLogFormatter>)logFormatter {
	self.logFormatter = logFormatter;
}

#pragma mark - 

+ (void)start {

	static dispatch_once_t predicate;

	dispatch_once(&predicate, ^{

		NSDictionary *levels = [JBCrashlyticsLogger levels];
		NSDictionary *settings = [JBCrashlyticsLogger settings];

		// add logger
		JBCrashlyticsLogger *cl = [[JBCrashlyticsLogger alloc] init];
#ifdef DEBUG
		cl.colorsEnabled = YES;
#else
		cl.colorsEnabled = NO;
#endif

		[DDLog addLogger:cl];

		NSUInteger customFlags = [JBCrashlyticsLogger customFlags];

		// set log levels for classes
		for (NSString *className in settings[@"classes"]) {

			NSString *logLevelString = settings[@"classes"][className];

			NSAssert(levels[logLevelString] != nil, @"unknown log level = %@", logLevelString);

			DDLogLevel level = [levels[logLevelString] unsignedIntegerValue];

			[DDLog setLevel:(level | customFlags) forClassWithName:className];
		}
	});
}

+ (NSDictionary*)levels {

	static NSDictionary *levels;
	static dispatch_once_t predicate;

	dispatch_once(&predicate, ^{

		// Log levels
		NSString *path = [[NSBundle mainBundle] pathForResource:@"JBCrashlyticsLoggerLevels" ofType:@"plist"];
		levels = [[NSDictionary alloc] initWithContentsOfFile:path];
	});

	return levels;
}

+ (NSDictionary*)settings {

	static NSDictionary *settings;
	static dispatch_once_t predicate;

	dispatch_once(&predicate, ^{

		// Class settings
		NSString *path = [[NSBundle mainBundle] pathForResource:@"JBCrashlyticsLoggerSettings" ofType:@"plist"];
		settings = [[NSDictionary alloc] initWithContentsOfFile:path];
	});

	return settings;
}

#warning TODO optimieren
+ (UIColor *)customColorWithFlag:(DDLogFlag)flag {

	NSDictionary *levels = [JBCrashlyticsLogger levels];
	NSDictionary *settings = [JBCrashlyticsLogger settings];

	for (NSString *level in levels) {

		DDLogFlag levelFlag = [levels[level] unsignedIntegerValue];

		// Das hier funktioniert nicht für Default-Levels, da Level != Flag ist.
		// Bei Custom-Levels ist das anderes -> Level == Flag
		if (levelFlag == flag) {

			NSNumber *colorValue = settings[@"colors"][level];

			if (colorValue != nil) {

				NSUInteger rgbValue = [colorValue unsignedIntegerValue];

				return [UIColor
								colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0
								green:((float)((rgbValue & 0xFF00) >> 8))/255.0
								blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0];
			}

			else {
				return nil;
			}
		}
	}

	return nil;
}

+ (NSUInteger)customFlags {

	NSDictionary *levels = [JBCrashlyticsLogger levels];
	NSDictionary *settings = [JBCrashlyticsLogger settings];

	// custom log flags
	NSUInteger customflags = 0;

	for (NSString *customFlagName in settings[@"globals"]) {

		if ([settings[@"globals"][customFlagName] boolValue] == YES) {

			NSAssert(levels[customFlagName] != nil, @"unknown log level = %@", customFlagName);

			NSUInteger flag = [levels[customFlagName] unsignedIntegerValue];

			customflags |= flag;
		}
	}

	return customflags;
}

+ (NSString*)replaceApiConstantsWithFieldNamesInString:(NSString*)string {

	static NSDictionary *replacementDict;
	static NSRegularExpression *regex;
	static dispatch_once_t predicate;

	__block NSError *error;

	dispatch_once(&predicate, ^{

		NSString *path = [[NSBundle mainBundle] pathForResource:@"JBCrashlyticsLoggerConstantReplacement" ofType:@"plist"];
		replacementDict = [[NSDictionary alloc] initWithContentsOfFile:path];

		regex = [NSRegularExpression regularExpressionWithPattern:@"(?<![0-9A-Z])([0-9A-Z]{3})(?![0-9A-Z])" options:0 error:&error];

	});


	if (error) {
		DDLogError(@"error = %@", [error localizedDescription]);
		return string;
	}

	NSMutableString *replacedString = [string mutableCopy];

	NSArray *matches = [regex matchesInString:string options:0 range:NSMakeRange(0, string.length)];

	NSUInteger numberOfMatches = matches.count;

	for (NSInteger i = numberOfMatches - 1; i >= 0; --i) {

		NSTextCheckingResult* match = matches[i];
		NSString *constant = [string substringWithRange:match.range];
		NSString *fieldName = [replacementDict objectForKey:constant];

		if (fieldName) {
			[replacedString replaceCharactersInRange:match.range withString:[NSString stringWithFormat:@"<$%@:(%@)$>", fieldName, constant]];
		}
	}

	return replacedString;
}

+ (NSString*)replaceTimestampsWithPrettyDateInString:(NSString*)string {

	static NSDateFormatter *dateFormatter;
	static NSNumberFormatter *numberFormater;
	static NSRegularExpression *regex;
	static dispatch_once_t predicate;

	__block NSError *error;

	dispatch_once(&predicate, ^{

		dateFormatter = [[NSDateFormatter alloc] init];
		[dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
		[dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"Europe/Berlin"]];

		numberFormater = [NSNumberFormatter new];

		regex = [NSRegularExpression regularExpressionWithPattern:@"(?<![0-9])(1[0-9]{9})(?![0-9])" options:0 error:&error];
	});


	if (error) {
		DDLogError(@"error = %@", [error localizedDescription]);
		return string;
	}

	NSMutableString *replacedString = [string mutableCopy];

	NSArray *matches = [regex matchesInString:string options:0 range:NSMakeRange(0, string.length)];

	NSUInteger numberOfMatches = matches.count;

	for (NSInteger i = numberOfMatches - 1; i >= 0; --i) {

		NSTextCheckingResult* match = matches[i];
		NSNumber *timestamp = [numberFormater numberFromString:[string substringWithRange:match.range]];
		NSDate *date = [NSDate dateWithTimeIntervalSince1970:[timestamp doubleValue]];


		if (date) {
			[replacedString replaceCharactersInRange:match.range withString:[NSString stringWithFormat:@"%@ <$%@$>", timestamp, [dateFormatter stringFromDate:date]]];
		}
	}

	return replacedString;
}

@end
