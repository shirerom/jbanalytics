//
//  JBCustomLogging.h
//  JBAnalytics
//
//  Created by Johannes Bauer on 11.03.15.
//  Copyright (c) 2015 Johannes Bauer. All rights reserved.
//

#import <CocoaLumberjack/CocoaLumberjack.h>
#ifndef JBCustomLogging_h
#define JBCustomLogging_h

typedef NS_OPTIONS(NSUInteger, JBLogFlag) {

	JBLogFlagError       = DDLogFlagError,   // 0...00001
	JBLogFlagWarning     = DDLogFlagWarning, // 0...00010
	JBLogFlagInfo        = DDLogFlagInfo,    // 0...00100
	JBLogFlagDebug       = DDLogFlagDebug,   // 0...01000
	JBLogFlagVerbose     = DDLogFlagVerbose, // 0...10000

	// custom
	JBLogFlagCustom0     = (1 << 10),
	JBLogFlagCustom1     = (1 << 11),
	JBLogFlagCustom2     = (1 << 12),
	JBLogFlagCustom3     = (1 << 13),
	JBLogFlagCustom4     = (1 << 14),
	JBLogFlagCustom5     = (1 << 15),
	JBLogFlagCustom6     = (1 << 16),
	JBLogFlagCustom7     = (1 << 17),
	JBLogFlagCustom8     = (1 << 18),
	JBLogFlagCustom9     = (1 << 19)
};

// das gibt's nicht wirklich
//#define LOG_LEVEL_APIRESPONSE (JBLogFlagApiResponse | LOG_FLAG_DEBUG)
//#define LOG_APIRESPONSE (ddLogLevel & JBLogFlagApiResponse)

#define DDLogCustom0(frmt, ...)  LOG_MAYBE(LOG_ASYNC_ENABLED, LOG_LEVEL_DEF, (DDLogFlag)JBLogFlagCustom0,   0, nil, __PRETTY_FUNCTION__, frmt, ##__VA_ARGS__)
#define DDLogCustom1(frmt, ...)  LOG_MAYBE(LOG_ASYNC_ENABLED, LOG_LEVEL_DEF, (DDLogFlag)JBLogFlagCustom1,   0, nil, __PRETTY_FUNCTION__, frmt, ##__VA_ARGS__)
#define DDLogCustom2(frmt, ...)  LOG_MAYBE(LOG_ASYNC_ENABLED, LOG_LEVEL_DEF, (DDLogFlag)JBLogFlagCustom2,   0, nil, __PRETTY_FUNCTION__, frmt, ##__VA_ARGS__)
#define DDLogCustom3(frmt, ...)  LOG_MAYBE(LOG_ASYNC_ENABLED, LOG_LEVEL_DEF, (DDLogFlag)JBLogFlagCustom3,   0, nil, __PRETTY_FUNCTION__, frmt, ##__VA_ARGS__)
#define DDLogCustom4(frmt, ...)  LOG_MAYBE(LOG_ASYNC_ENABLED, LOG_LEVEL_DEF, (DDLogFlag)JBLogFlagCustom4,   0, nil, __PRETTY_FUNCTION__, frmt, ##__VA_ARGS__)
#define DDLogCustom5(frmt, ...)  LOG_MAYBE(LOG_ASYNC_ENABLED, LOG_LEVEL_DEF, (DDLogFlag)JBLogFlagCustom5,   0, nil, __PRETTY_FUNCTION__, frmt, ##__VA_ARGS__)
#define DDLogCustom6(frmt, ...)  LOG_MAYBE(LOG_ASYNC_ENABLED, LOG_LEVEL_DEF, (DDLogFlag)JBLogFlagCustom6,   0, nil, __PRETTY_FUNCTION__, frmt, ##__VA_ARGS__)
#define DDLogCustom7(frmt, ...)  LOG_MAYBE(LOG_ASYNC_ENABLED, LOG_LEVEL_DEF, (DDLogFlag)JBLogFlagCustom7,   0, nil, __PRETTY_FUNCTION__, frmt, ##__VA_ARGS__)
#define DDLogCustom8(frmt, ...)  LOG_MAYBE(LOG_ASYNC_ENABLED, LOG_LEVEL_DEF, (DDLogFlag)JBLogFlagCustom8,   0, nil, __PRETTY_FUNCTION__, frmt, ##__VA_ARGS__)
#define DDLogCustom9(frmt, ...)  LOG_MAYBE(LOG_ASYNC_ENABLED, LOG_LEVEL_DEF, (DDLogFlag)JBLogFlagCustom9,   0, nil, __PRETTY_FUNCTION__, frmt, ##__VA_ARGS__)
//#define DDLogApiResponse(frmt, ...) ASYNC_LOG_OBJC_MAYBE(ddLogLevel, JBLogFlagApiResponse, 0, frmt, ##__VA_ARGS__)

#endif
