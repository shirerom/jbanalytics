//
//  JBTracking.h
//  JBAnalytics
//
//  Created by Johannes Bauer on 30.12.14.
//  Copyright (c) 2014 Johannes Bauer. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JBTracking : NSObject

+ (void)start;
+ (void)startCrashlytics;
+ (void)startGoogleAnalytics;

@end
