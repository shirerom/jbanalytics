//
//  JBTracking.m
//  JBAnalytics
//
//  Created by Johannes Bauer on 30.12.14.
//  Copyright (c) 2014 Johannes Bauer. All rights reserved.
//

#import <GoogleAnalytics/GAI.h>
#import "JBCrashlyticsLogger.h"
#import "JBTracking.h"


@implementation JBTracking

+ (void)start {

	[JBTracking startCrashlytics];
	[JBTracking startGoogleAnalytics];
}

+ (void)startCrashlytics {

	NSDictionary *fabric = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"Fabric"];
	NSString *key = [fabric objectForKey:@"APIKey"];
	
	NSAssert((key != nil && [key isKindOfClass:[NSString class]] && key.length > 0), @"Fabric.APIKey doesn't exist in Info.plist");

	// Crashlytics
	[Crashlytics startWithAPIKey:key];

	// Logging
	[JBCrashlyticsLogger start];

	DDLogError(@"CrashlyticsLogger started");
}

+ (void)startGoogleAnalytics {

	NSString *key = [[JBTracking infoPlistDict] objectForKey:@"GoogleAnalyticsTrackerId"];

	NSAssert((key != nil && [key isKindOfClass:[NSString class]] && key.length > 0), @"JBTracking.GoogleAnalyticsTrackerId doesn't exist in Info.plist");
	
	// Optional: automatically send uncaught exceptions to Google Analytics.
	[GAI sharedInstance].trackUncaughtExceptions = NO;

	// Optional: set Google Analytics dispatch interval to e.g. 20 seconds.
#ifdef DEBUG
	[GAI sharedInstance].dryRun = YES;
	[GAI sharedInstance].dispatchInterval = 10;
#else
	[GAI sharedInstance].dispatchInterval = 60;
#endif

	// Optional: set Logger to VERBOSE for debug information.
	[[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelInfo];

	// Initialize tracker. Replace with your tracking ID.
	[[GAI sharedInstance] trackerWithTrackingId:key];

	DDLogError(@"GoogleAnalytics started");
}

+ (NSDictionary *)infoPlistDict {
	return [[NSBundle mainBundle] objectForInfoDictionaryKey:@"JBTracking"];
}

@end
