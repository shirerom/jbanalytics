# Dependancies

* Google Analytics SDK
* Crashlytics (Fabric)
* CocoaLumberjack

## Pods

```
#!pods
pod 'CocoaLumberjack', '~> 2.0'
pod 'GoogleAnalytics'
```

# Installation

add git-submodule and drag files into Xcode project

# Setup

to your project's Info.plist

* add your Crashlytics-Key: JBTrackingCrashlyticsAPIKey
* add your Google Analytics TrackerId: JBTrackingGoogleAnalyticsTrackerId

## Custom Log Levels

... coming soon ...